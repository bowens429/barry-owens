class Post < ActiveRecord::Base
	# Create a many comments for one post relationship
	# Make the comments dependent upon the post,
	# so that all comments are destroyed when
	# a post is destroyed.
	has_many :comments, dependent: :destroy
	# validate the presence of post title
	validates_presence_of :title
	# validate the presence of post body
	validates_presence_of :body
end
