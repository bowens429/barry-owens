class Comment < ActiveRecord::Base
	# Associate the comments to the parent post.
	belongs_to :post
	# validate presence of post id
	validates_presence_of :post_id
	# validate presence of comment body
	validates_presence_of :body 
end
